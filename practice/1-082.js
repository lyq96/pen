/**
 * Created by lenovo on 2017/8/7.
 */
var year = window.prompt("请输入一个年份")
/*闰年:整百年份能被400整除(能被400整除，也就能被100整除)，非整百年份不能被100整除，但能被4整除*/
if(year % 4 === 0 && year % 100 !== 0 || year % 400 === 0 ){
    alert("是闰年");
}
else{
    alert("不是闰年");
}
